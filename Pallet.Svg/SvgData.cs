﻿using System;

namespace Pallet.Svg
{
    /// <summary>
    /// Represents the output of Pallet.Svg.Builder
    /// </summary>
    public class SvgData
    {
        /// <summary>
        /// Represents the version of the Pallet.Svg.Builder format
        /// </summary>
        public float Version;

        /// <summary>
        /// Contains the original Svg
        /// </summary>
        public string Svg;
    }
}
