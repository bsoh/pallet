using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Svg;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Xml;
using System.IO;

namespace Pallet.Svg
{
    /// <summary>
    /// Provides utilities to utilise SVG images
    /// </summary>
    public static class SvgUtil
    {
        /// <summary>
        /// Generates a <see cref="Texture2D"> with the specified parameters
        /// </summary>
        /// <remarks>
        /// Note that this can be heavy on CPU and RAM so should ideally be used once, in conjunction with some kind of a sprite manager
        /// </remarks>
        /// <param name="graphics">The <see cref="GraphicsDevice"> to use for rendering</param>
        /// <param name="svgData">The <see cref="SvgData"> to render</param>
        /// <param name="width">The desired output width</param>
        /// <param name="height">The desired output height</param>
        /// <param name="shapeRendering">The desired <see cref="SvgShapeRendering"> method to use</param>
        /// <param name="format">The desired <see cref="ImageFormat"> to render the SVG as</param>
        /// <returns>A <see cref="Texture2D"> generated from the input SVG</returns>
        public static Texture2D Generate(GraphicsDevice graphics, SvgData svgData, int width, int height, SvgShapeRendering shapeRendering, ImageFormat format)
        {
            // Check versioning
            if (svgData.Version == 1.0)
            {
                string decoded = HttpUtility.HtmlDecode(svgData.Svg);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(decoded);
                SvgDocument doc = SvgDocument.Open(xml);
                doc.ShapeRendering = shapeRendering;
                Bitmap bitmap = doc.Draw(width, height);
                Texture2D tex;
                MemoryStream stream = new MemoryStream();
                bitmap.Save(stream, format);
                tex = Texture2D.FromStream(graphics, stream);
                return tex;
            }
            else
            {
                // Version is not supported
                throw new Exception("Pallet.Svg.Builder version is not supported by this version of the library, Svg is: " + svgData.Version);
            }
        }
    }
}