# Pallet.Svg
Provides a wrapper to render SVGs prepared with Pallet.Svg.Builder.

## Installation
1. Add the nuget package to your project
2. Add a reference to `Pallet.Svg.dll` in the MonoGame Content Pipeline (should be in your `packages` folder, otherwise just download directly)

# Pallet.Svg.Builder
Prepares Scalable Vector Graphic images (.svg) for the Pallet.Svg library by converting it to a custom XML format which is loadable through the Content Pipeline as XML.

## Usage
````
$ SvgBuild <input.svg> <output.xml>
````
or
````
$ SvgBuild <input.svg>
````
(will output to input.svg.xml)
or
````
$ SvgBuild -a
$ SvgBuild --all
````
(Will build all in current directory and sub directories)
or
````
$ SvgBuild
````
(Will display help message)

## Contributing
See the [contribution guidelines]("https://gitlab.com/bsoh/pallet/pallet/-/blob/develop/contributing.md") in the main Pallet repo for details.