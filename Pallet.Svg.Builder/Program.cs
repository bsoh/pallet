﻿using System;
using System.IO;
using System.Xml.Linq;
using Svg;
using System.Xml;

namespace SVG_Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            Log(LogTypes.Log, "Pallet.Svg.Builder v0.1; Copyright (c) 2020 Big Stack of Hay");
            switch (args.Length)
            {
                case 0:
                    Help();
                    break;
                case 1:
                    if (args[0] == "--all" || args[0] ==  "-a")
                        BuildAll();
                    else
                        RW(args[0], args[0] + ".xml");
                    break;
                case 2:
                    RW(args[0], args[1]);
                    break;
            }
        }

        static void Help ()
        {
            Log(LogTypes.Log, "Pallet.SVG.Builder");
            Log(LogTypes.Log, "Usage:");
            Log(LogTypes.Log, "SvgBuild <input.svg> [output.xml]");
            Log(LogTypes.Log, "Converts SVG file into usable XML file for Pallet.Svg Library");
            Log(LogTypes.Log, "Arguments:");
            Log(LogTypes.Log, "<input.svg>  : The SVG file to prepare");
            Log(LogTypes.Log, "[output.xml] : The XML file to write to (if left blank will write to input.svg.xml");
        }

        static void RW(string inFile, string outFile)
        {
            Log(LogTypes.Starting, "Building " + inFile + " to " + outFile);

            SvgDocument svg;
            svg = new SvgDocument();
            try
            {
                svg = SvgDocument.Open(inFile);
            }
            catch (Exception e)
            {
                Log(LogTypes.Error, "Encountered Exception when attempting to load the SVG: " + e.ToString());
                return;
            }
            // Asset node
            XElement Asset = new XElement("Asset");
            Asset.SetAttributeValue("Type", "Pallet.Svg.SvgData");

            // Add versioning
            Asset.SetElementValue("Version", 1.0);

            XmlDocument xml = new XmlDocument();
            xml.Load(inFile);

            // Encode XML
            string encodedXML = System.Web.HttpUtility.HtmlEncode(xml.OuterXml);
            
            Asset.SetElementValue("Svg", encodedXML);

            // XnaContent node
            XNamespace xNamespace = "Microsoft.Xna.Framework";
            XElement XnaContent = new XElement("XnaContent", Asset);
            XnaContent.SetAttributeValue(XNamespace.Xmlns + "ns", xNamespace);

            // Creation of the XDocument
            XDocument Xdoc = new XDocument(XnaContent);
            // Write to File
            Xdoc.Save(outFile);

            Log(LogTypes.Successful, "Built Svg to " + outFile);
        }

        public enum LogTypes
        {
            Log,
            Error,
            Successful,
            Starting
        }

        static void Log(LogTypes type, string msg) 
        {
            string Beginning = "";
            switch (type)
            {
                case LogTypes.Log:
                    Beginning = "";
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case LogTypes.Error:
                    Beginning = "[Error     ] ";
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogTypes.Successful:
                    Beginning = "[Successful] ";
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case LogTypes.Starting:
                    Beginning = "[Starting  ] ";
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }

            Console.Write(Beginning);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(msg);
        }
    
        static void BuildAll()
        {
            DirectoryInfo directory = new DirectoryInfo(Directory.GetCurrentDirectory());
            FileInfo[] files = directory.GetFiles("*.svg", SearchOption.AllDirectories);

            foreach (FileInfo file in files)
            {
                RW(file.FullName, file.FullName + ".xml");
            }
        }
    }
}
